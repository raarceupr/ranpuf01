from pylibftdi import Device, Driver, INTERFACE_B

def hexVal(st):
    total = 0
    for c in st:
        total = total << 8
        total = total + ord(c)
    return total


with Device(mode='t',interface_select=INTERFACE_B) as dev:
    dev.baudrate = 115200
    dev.write('a')
    st = ''
    while len(st) < 4:
       st = st +  dev.read(1)
    #print st
    print ":".join("{:02x}".format(ord(c)) for c in st)
    print hexVal(st)

