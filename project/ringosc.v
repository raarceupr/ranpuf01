`timescale 1ns / 1ps

//module ringosc(input a, output  y );
//  wire [4:0] tmp;
//  
//  LUT5 #( .INIT(32'h00000007)) LUT5_NAND ( .O(y), .I0(a), .I1(1'b1), .I2(1'b0), .I3(1'b0), .I4(1'b0) );
//
//endmodule

module top(input  a, output [7:0] c);

  genvar i;
  generate
  for (i = 0; i < 8; i = i + 1) begin: top_gen_block
    ringosc RO_00(.a(a), .c(c[i]));
  end
  endgenerate

endmodule



module topOLD(input  a, output [7:0] c);

  genvar i;
  generate
  for (i = 0; i < 8; i = i + 1) begin: old_top_gen_block
    ringosc RO_00(.a(a), .c(c[i]));
  end
  endgenerate

endmodule



module ringosc(input en, output y);
  (* KEEP = "TRUE" *)
  wire [8:0] tmp;  
  nand(tmp[1], en, tmp[0]);
  
  genvar i;
  generate
    for (i = 1; i <= 8; i = i + 1) begin: ringosc_gen_block
		not NOT00(tmp[(i+1)%9], tmp[i%9]);
    end
  endgenerate  
  
  assign  y = tmp[1];

endmodule

module ringosc_w_freq_divider(input a, output c);
  (* KEEP = "TRUE" *)
  wire [8:0] tmp;
  (* KEEP = "TRUE" *)
  reg [10:0] cnt;
  
  nand(tmp[1], a, tmp[0]);
  
  genvar i;
  generate
    for (i = 1; i <= 8; i = i + 1) begin: ringosc_gen_block
      not NOT00(tmp[(i+1)%9], tmp[i%9]);
    end
  endgenerate  
  
  assign  y = tmp[1];

  always @(posedge y) cnt <= cnt + 1'b1;
  
  assign c = cnt[10];
  
endmodule
  


module ringosc_LUTS(input a, output c);
  (* KEEP = "TRUE" *)
  wire [8:0] tmp;
  (* KEEP = "TRUE" *)
  reg [10:0] cnt;
  LUT5 #( .INIT(32'h00000007)) LUT5_NAND ( .O(tmp[1]), .I0(a), .I1(tmp[0]), .I2(1'b0), .I3(1'b0), .I4(1'b0) );
  LUT5 #( .INIT(32'h00000001)) LUT5_NOT00  ( .O(tmp[2]), .I0(tmp[1]), .I1(1'b0), .I2(1'b0), .I3(1'b0), .I4(1'b0) );
  LUT5 #( .INIT(32'h00000001)) LUT5_NOT01  ( .O(tmp[3]), .I0(tmp[2]), .I1(1'b0), .I2(1'b0), .I3(1'b0), .I4(1'b0) );
  LUT5 #( .INIT(32'h00000001)) LUT5_NOT02  ( .O(tmp[4]), .I0(tmp[3]), .I1(1'b0), .I2(1'b0), .I3(1'b0), .I4(1'b0) );
  LUT5 #( .INIT(32'h00000001)) LUT5_NOT03  ( .O(tmp[5]), .I0(tmp[4]), .I1(1'b0), .I2(1'b0), .I3(1'b0), .I4(1'b0) );

  LUT5 #( .INIT(32'h00000001)) LUT5_NOT04  ( .O(tmp[6]), .I0(tmp[5]), .I1(1'b0), .I2(1'b0), .I3(1'b0), .I4(1'b0) );
  LUT5 #( .INIT(32'h00000001)) LUT5_NOT05  ( .O(tmp[7]), .I0(tmp[6]), .I1(1'b0), .I2(1'b0), .I3(1'b0), .I4(1'b0) );
  LUT5 #( .INIT(32'h00000001)) LUT5_NOT06  ( .O(tmp[8]), .I0(tmp[7]), .I1(1'b0), .I2(1'b0), .I3(1'b0), .I4(1'b0) );
  LUT5 #( .INIT(32'h00000001)) LUT5_NOT07  ( .O(tmp[0]), .I0(tmp[8]), .I1(1'b0), .I2(1'b0), .I3(1'b0), .I4(1'b0) );
  
  assign  y = tmp[1];

  
  always @(posedge y) cnt <= cnt + 1'b1;
  
  assign c = cnt[10];

endmodule

module decoder_256(input enable, input [7:0] a, output  [255:0] y) ;
	assign y = enable ? (1 << a):0;
endmodule

module mux_256_to_1(input [255:0] a, input [7:0] sel, output y);
	assign y = a[sel];
endmodule


module decoder_64(input enable, input [5:0] a, output  [63:0] y) ;
  assign y = enable ? (1 << a):0;
endmodule

module mux_64_to_1(input [63:0] a, input [5:0] sel, output y);
  assign y = a[sel];
endmodule