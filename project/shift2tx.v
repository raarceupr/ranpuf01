`include "uart_defs.v"

// Module shift2tx
// This module receives a 4byte data_in and presents it on its
// output byte by byte (starting with the least significant).
//
// 1) provide data_in and enable ld.
// 2) shift to obtain the other bytes.


module  shift2tx (
    input wire        	clk,
    input wire        	rst,
    output reg [7:0]  	dout,
    output reg 			valid,
    input wire [31:0] 	data_in,
    input wire        	load,
    input wire        	shift
);

parameter [1:0] SHF_START  = 2'd0;
parameter [1:0] SHF_DATA   = 2'd1;


reg [31:0] data    = 31'd0;
reg [1:0]  state   = SHF_START;
reg [1:0]  bit_cnt = 2'd0;

// I am delaying the valid signal because if not it will be set 
// **before** the actual valid data

reg valid_del = 1'b0;

always  @ (posedge clk)
begin
    if (rst)
    begin
        state <= SHF_START;
        dout  <= 8'b0;
        valid <= 1'b0;
		  valid_del <= 1'b0;
    end

    else
    begin
        // Default assignments
		  valid <= valid_del;
        dout <= dout;
        state <= state;
        bit_cnt <= bit_cnt;

        case(state)
				// In this state, we are waiting for the load signal
            SHF_START:
            begin
                if(load)
                begin
                    data    	<= data_in;
                    state   	<= SHF_DATA;
                    bit_cnt 	<= 1'd0;
                    valid_del <= 1'b1;
                end
            end

            SHF_DATA:
				begin
					//dout <= data[7:0];
					dout <= data[31:24];
					valid_del <= 1'b0;
					if(shift)
					begin
						 bit_cnt <= (bit_cnt + 1'd1);
						 //data <= {7'b0, data[31:8]};
						 data <= {data[23:0], 8'b0};
						 valid_del <= 1'b1;
						 
						 if(bit_cnt == 3'd3)
						 begin
							  valid_del <= 1'd0;
							  state <= SHF_START;
						 end
					end
				end
            default:
                $display ("SHIFTER TX: Invalid state 0x%X", state);

        endcase
    end
end

endmodule
