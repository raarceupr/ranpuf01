//
// 

module papilio_top
(
    input wire          clk,
    input wire          ftdi_rx,
    output wire         ftdi_tx,
    output reg          led = 1'b1
	 //,
    //output wire [15:0]  debug_wing
);

// Combinatorial logic
//assign debug_wing = {13'd0, clk, ftdi_rx, ftdi_tx};

// Module instantiation


wire toggle;
wire [7:0] w_rx_dout;
wire w_rx_valid, start_puf;
//reg [31:0] ro_cnt = 0;
reg ro_clear = 0;
wire  [63:0] ro_enable;// = 0;
reg ro_enable_master = 0;
wire [63:0] ro_output;
wire [5:0] comparison;

reg tx_enable = 0;
assign toggle = ( w_rx_dout == 8'h41 && w_rx_valid);
assign start_puf = w_rx_valid;

// instance of the receiver

uart_rx uart_rx_00 (
    .clk(clk),
    .rst(1'b0),
    .din(ftdi_rx),
    .data_out(w_rx_dout),
    .valid(w_rx_valid)
);

wire [3:0] cntr_index = w_rx_dout[3:0];
shiftAndTx stx00(
	.clk(clk),
	.rst(1'b0),
	.data_in({ro_cnt[0],10'b0,comparison}),
	.load(tx_enable),
	.tx_out(ftdi_tx)
);



genvar i;

wire [5:0] decoder_input [11:0];
wire [255:0] ro_chosen [11:0];
wire [15:0] mux_output;

wire [15:0] ro_cnt[11:0];

generate
  for (i = 0; i < 12; i = i + 1) begin: dec_mux_cnt_gen_block
    decoder_64 DECODER(.enable(ro_enable_master), .a(decoder_input[i]), .y(ro_chosen[i]));
    mux_64_to_1 MUX (.a(ro_output), .sel(decoder_input[i]), .y(mux_output[i]));
    counter_for_ros COUNT_RO(.rst(ro_clear), .clk(mux_output[i]), .y(ro_cnt[i]));
  end
endgenerate

generate
  for (i = 0; i < 6; i = i + 1) begin: comparison_gen_block
    assign comparison[i] = ( ro_cnt[i*2] > ro_cnt[i*2 + 1] ); 
  end
endgenerate

generate
  for (i = 0; i < 64; i = i + 1) begin: ringosc_gen_block
    ringosc RO_00(.en(ro_enable[i]), .y(ro_output[i]));
    assign ro_enable[i] = ro_chosen[0][i] | ro_chosen[1][i] | ro_chosen[2][i] | ro_chosen[3][i] | 
      ro_chosen[4][i]  | ro_chosen[5][i]  | ro_chosen[6][i] | ro_chosen[7][i] | ro_chosen[8][i] | ro_chosen[9][i] |
      ro_chosen[10][i] | ro_chosen[11][i]; // | ro_chosen[12][i] | ro_chosen[13][i] | ro_chosen[14][i] | ro_chosen[15][i] ;  
  end
endgenerate

/*
generate
  for (i = 0; i < 8; i = i + 1) begin
    decoder_256(.a(decoder_input[i]), .y(ro_chosen[i]));
  end
endgenerate
*/


assign decoder_input[0] = w_rx_dout;
assign decoder_input[1] = {w_rx_dout[6:1]};
assign decoder_input[2] = {w_rx_dout[7:2]};
assign decoder_input[3] = {w_rx_dout[7:4], w_rx_dout[1:0] };
assign decoder_input[4] = {w_rx_dout[7:6], w_rx_dout[3:0] };
assign decoder_input[5] = {w_rx_dout + 1'd5};
assign decoder_input[6] = {w_rx_dout};
assign decoder_input[7] = {w_rx_dout + 1'd7};
assign decoder_input[8] = {w_rx_dout};
assign decoder_input[9] = {w_rx_dout + 1'd9};
assign decoder_input[10] = {w_rx_dout};
assign decoder_input[11] ={w_rx_dout + 1'd11};
// assign decoder_input[12] = {w_rx_dout[7:4], w_rx_dout[7:4]};
// assign decoder_input[13] = {w_rx_dout[7:4], w_rx_dout[7:4]};
// assign decoder_input[14] = {w_rx_dout[7:4], w_rx_dout[7:4]};
// assign decoder_input[15] = {w_rx_dout[7:4], w_rx_dout[7:4]};

/*
ringosc RO_00(.en(ro_enable), .y(ro_output));

always @(posedge ro_output or posedge ro_clear) begin
	if (ro_clear)  ro_cnt <= 0;
	else           ro_cnt <= ro_cnt + 1'b1;
end
*/

always @(posedge clk) begin
	if (toggle) led <= ~led;
end



// FSM here


localparam [1:0] WAIT_FOR_LETER  = 0,
                 COUNT_OSC       = 1,
					  SEND_RESULT     = 2;

reg [1:0] state   = WAIT_FOR_LETER;
reg [9:0] cnt     = 0;


always  @ (posedge clk)
begin


        // Default assignments
		  state <= state;
		  ro_enable_master  <= 0;
		  cnt <= cnt + 1'd1;
		  tx_enable <= 0;
		  ro_clear <= 1;
        case(state)
            WAIT_FOR_LETER: begin
                if(start_puf) begin
                    ro_enable_master <= 0;
                    state  <= COUNT_OSC;
                    cnt 	<= 0;
					ro_clear <= 0;
                end
            end

            COUNT_OSC: begin
				   ro_enable_master <= 1;
				   ro_clear <= 0;
				   if (cnt == 10'b1111111111) begin
					  state <= SEND_RESULT;
					  ro_enable_master <= 0;
					  cnt <= 0;
					  //ro_clear <= 0;
					end
				end
				
            SEND_RESULT: begin
				   tx_enable <= 1;
					state <= WAIT_FOR_LETER;
					ro_clear <= 0;
					cnt <= 0;
				end
            default:
                $display ("SHIFTER TX: Invalid state 0x%X", state);

        endcase
   
end



endmodule

// TRASH
/*
uart_tx uart_tx_00 (
	.clk(clk),
	.rst(1'b0),
	.dout(ftdi_tx),
	.data_in(w_rx_dout ^ 8'h20),
	.en(w_rx_valid)
);
*/

module counter_for_ros(input rst, input clk, output reg [15:0] y = 0);
  always @(posedge clk or posedge rst) begin
    if (rst)  y <= 0;
    else      y <= y + 1'b1;
  end
endmodule