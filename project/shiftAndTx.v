`timescale 1ns / 1ps
module shiftAndTx(
	input wire 			clk,
	input wire 			rst,
	input wire [31:0]	data_in,
	input wire 			load,
	output wire 		tx_out
);

wire [7:0] w_shift_dout;
wire  w_shift_valid;
wire  w_uart_ready;


// converting the uart_ready from a long pulse to 
// a 1 period pulse

reg uart_ready_delay = 1'd0;
reg uart_ready_pulse = 1'd0;

always @(posedge clk) begin
	uart_ready_delay <= w_uart_ready;
	if (uart_ready_delay == 1'b0 && w_uart_ready == 1'b1)
		uart_ready_pulse <= 1'b1;
	else
		uart_ready_pulse <= 1'b0;
end

shift2tx shift2tx_00 (
    .clk        (clk),
    .rst        (rst),
    .dout       (w_shift_dout),
    .valid      (w_shift_valid),
    .data_in    (data_in),
    .load       (load),
    .shift      (uart_ready_pulse)
);

uart_tx uart_tx_00 (
	.clk			(clk),
	.rst			(rst),
	.dout			(tx_out),
	.en			(w_shift_valid),
	.data_in		(w_shift_dout),
	.rdy			(w_uart_ready)
);

endmodule
